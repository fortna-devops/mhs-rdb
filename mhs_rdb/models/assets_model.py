from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, ForeignKey, String, DateTime
from sqlalchemy.orm import relationship

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, IdMixin, TimestampedMixin, ToDisplayMixin


if TYPE_CHECKING:
    from . import ComponentsModel, FacilityAreasModel, AssetTypesModel, AssetStatusTransitionsModel


__all__ = ['AssetsModel']


class AssetsModel(TableNameMixin, IdMixin, TimestampedMixin, ToDisplayMixin, BaseSiteModel):
    facility_area_id = Column(Integer, ForeignKey('facility_areas.id', ondelete='CASCADE'), nullable=False)
    type_id = Column(Integer, ForeignKey('asset_types.id', ondelete='CASCADE'), nullable=False)
    name = Column(String, nullable=False)
    install_date = Column(DateTime)

    facility_area = relationship('FacilityAreasModel')
    components = relationship('ComponentsModel')
    type = relationship('AssetTypesModel')
    status_transitions = relationship('AssetStatusTransitionsModel')
