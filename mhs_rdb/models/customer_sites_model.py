from sqlalchemy import Column, Float, String, Text

from .mixins import TimestampedMixin, TableNameMixin
from .base_model import BaseCustomerModel


__all__ = ['CustomerSitesModel']


class CustomerSitesModel(TableNameMixin, TimestampedMixin, BaseCustomerModel):

    name = Column(String, primary_key=True)
    description = Column(Text)
    nation = Column(String)
    region = Column(String)
    facility_name = Column(String, nullable=False)
    schema_name = Column(String, unique=True, nullable=False)
    latitude = Column(Float)
    longitude = Column(Float)
    timezone = Column(String, server_default='Etc/UTC', nullable=False)
