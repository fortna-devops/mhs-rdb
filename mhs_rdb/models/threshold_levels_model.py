from typing import TYPE_CHECKING

from sqlalchemy import Column, String, Enum
from sqlalchemy.orm import relationship

from .enums import ColorsEnum
from .base_model import BaseSiteModel
from .mixins import TableNameMixin, TimestampedMixin

if TYPE_CHECKING:
    from . import ThresholdsModel


__all__ = ['ThresholdLevelsModel']


class ThresholdLevelsModel(TableNameMixin, TimestampedMixin, BaseSiteModel):
    name = Column(String, nullable=False, primary_key=True)
    color = Column(Enum(ColorsEnum), nullable=False)

    thresholds = relationship('ThresholdsModel')
