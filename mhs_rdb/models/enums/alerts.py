import enum


__all__ = ['AlertRolesEnum']


class AlertRolesEnum(enum.Enum):
    developer = 1
    support = 2
    customer = 3
