from typing import TYPE_CHECKING

from sqlalchemy import Column, ForeignKey, Integer, Float, String
from sqlalchemy.orm import relationship

from .mixins import TableNameMixin, IdMixin, TimestampedMixin
from .base_model import BaseSiteModel

if TYPE_CHECKING:
    from . import MetricsModel, ThresholdTypesModel, ThresholdLevelsModel


__all__ = ['ThresholdsModel']


class ThresholdsModel(TableNameMixin, IdMixin, TimestampedMixin, BaseSiteModel):
    metric_id = Column(Integer, ForeignKey('metrics.id', ondelete='CASCADE'), nullable=False)
    type_name = Column(String, ForeignKey('threshold_types.name', ondelete='CASCADE'), nullable=False)
    level_name = Column(String, ForeignKey('threshold_levels.name', ondelete='CASCADE'), nullable=False)
    value = Column(Float, nullable=False)

    metric = relationship('MetricsModel')
    type = relationship('ThresholdTypesModel')
    level = relationship('ThresholdLevelsModel')
