from typing import TYPE_CHECKING

from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, TimestampedMixin

if TYPE_CHECKING:
    from . import ThresholdsModel


__all__ = ['ThresholdTypesModel']


class ThresholdTypesModel(TableNameMixin, TimestampedMixin, BaseSiteModel):
    name = Column(String, nullable=False, primary_key=True)

    thresholds = relationship('ThresholdsModel')
