from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, ForeignKey, String, Float, Boolean, CheckConstraint
from sqlalchemy.orm import relationship
from sqlalchemy.sql import column, or_

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, IdMixin, TimestampedMixin, ToDisplayMixin
from .data_models import RAW_DATA_TABLES, CALCULATED_DATA_TABLES


if TYPE_CHECKING:
    from . import DataSourcesModel, ThresholdsModel, RawFloatDataModel, RawIntDataModel, RawStringDataModel,\
        CalculatedFloatDataModel, CalculatedIntDataModel, CalculatedStringDataModel, AlarmsModel


__all__ = ['MetricsModel']


class MetricsModel(TableNameMixin, IdMixin, TimestampedMixin, ToDisplayMixin, BaseSiteModel):
    data_source_id = Column(Integer, ForeignKey('data_sources.id', ondelete='CASCADE'), nullable=False)
    name = Column(String, nullable=False)
    display_name = Column(String, nullable=False)
    to_analyze = Column(Boolean, nullable=False, server_default='TRUE')
    units = Column(String)
    min_value = Column(Float)
    max_value = Column(Float)

    raw_data_table = Column(String)
    calculated_data_table = Column(String)

    thresholds = relationship('ThresholdsModel')
    alarms = relationship('AlarmsModel')

    raw_float_data = relationship('RawFloatDataModel')
    raw_int_data = relationship('RawIntDataModel')
    raw_string_data = relationship('RawStringDataModel')

    calculated_float_data = relationship('CalculatedFloatDataModel')
    calculated_int_data = relationship('CalculatedIntDataModel')
    calculated_string_data = relationship('CalculatedStringDataModel')

    data_source = relationship('DataSourcesModel')

    __table_args__ = (
        CheckConstraint(or_(  # type: ignore
            column('raw_data_table').in_(RAW_DATA_TABLES),
            column('calculated_data_table').in_(CALCULATED_DATA_TABLES)
        ), name='data_table_check'),
    )
