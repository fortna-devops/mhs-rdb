from typing import TYPE_CHECKING

from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from .mixins import TableNameMixin, TimestampedMixin
from .base_model import BaseSiteModel

if TYPE_CHECKING:
    from . import AlarmsModel, AlarmConfigsModel


__all__ = ['AlarmTypesModel']


class AlarmTypesModel(TableNameMixin, TimestampedMixin, BaseSiteModel):
    name = Column(String, nullable=False, primary_key=True)

    configs = relationship('AlarmConfigsModel')
