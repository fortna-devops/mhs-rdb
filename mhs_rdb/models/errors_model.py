from sqlalchemy import Column, String

from .mixins import TableNameMixin, CreatedAtMixin
from .base_model import BaseSiteModel


__all__ = ['ErrorsModel']


class ErrorsModel(TableNameMixin, CreatedAtMixin, BaseSiteModel):

    name = Column(String, primary_key=True)
    description = Column(String, nullable=False)
