from typing import TYPE_CHECKING

from sqlalchemy import Float, Column, Integer, ForeignKey, DateTime, UniqueConstraint
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, IdMixin, CreatedAtMixin


if TYPE_CHECKING:
    from . import ComponentsModel


__all__ = ['ComponentHealthModel']


class ComponentHealthModel(TableNameMixin, IdMixin, CreatedAtMixin, BaseSiteModel):

    @declared_attr
    def __table_args__(cls):  # pylint: disable=E0213
        return (
            UniqueConstraint('component_id', 'timestamp'),
        )

    component_id = Column(Integer, ForeignKey('components.id', ondelete='CASCADE'), nullable=False)
    timestamp = Column(DateTime, nullable=False)
    health = Column(Float, nullable=False)

    component = relationship('ComponentsModel')
