from sqlalchemy import Column, Boolean


__all__ = ['ToDisplayMixin']


class ToDisplayMixin:
    to_display = Column(Boolean, nullable=False, server_default='TRUE')
