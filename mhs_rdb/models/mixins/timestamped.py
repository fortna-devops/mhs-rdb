from sqlalchemy import Column, DateTime, func


__all__ = ['CreatedAtMixin', 'TimestampedMixin']


class CreatedAtMixin:
    created_at = Column(DateTime, nullable=False, default=func.now())


class TimestampedMixin(CreatedAtMixin):
    updated_at = Column(DateTime, onupdate=func.now())
