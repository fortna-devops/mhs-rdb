from sqlalchemy import Column, Integer, BigInteger
from sqlalchemy.ext.declarative import declared_attr


__all__ = ['IdMixin', 'BigIdMixin']


class IdMixin:

    @declared_attr
    def id(cls):  # pylint: disable=E0213,C0103
        return Column('id', Integer, primary_key=True)


class BigIdMixin:

    @declared_attr
    def id(cls):  # pylint: disable=E0213,C0103
        return Column('id', BigInteger, primary_key=True)
