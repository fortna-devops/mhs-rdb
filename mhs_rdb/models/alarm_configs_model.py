from typing import TYPE_CHECKING

from sqlalchemy import Column, ForeignKey, String, Enum, Integer
from sqlalchemy.orm import relationship

from .mixins import TableNameMixin, TimestampedMixin, ToDisplayMixin
from .enums import ColorsEnum
from .base_model import BaseSiteModel

if TYPE_CHECKING:
    from . import AlarmsModel, AlarmTypesModel


__all__ = ['AlarmConfigsModel']


class AlarmConfigsModel(TableNameMixin, TimestampedMixin, ToDisplayMixin, BaseSiteModel):
    type_name = Column(String, ForeignKey('alarm_types.name', ondelete='CASCADE'), nullable=False, primary_key=True)
    color = Column(Enum(ColorsEnum), nullable=False, primary_key=True)
    weight = Column(Integer, nullable=False, server_default='1')

    type = relationship('AlarmTypesModel')
    alarms = relationship('AlarmsModel')
