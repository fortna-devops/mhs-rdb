from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, ForeignKey, String, DateTime
from sqlalchemy.orm import relationship

from .base_model import BaseSiteModel
from .mixins import TableNameMixin, IdMixin, TimestampedMixin, ToDisplayMixin


if TYPE_CHECKING:
    from . import AssetsModel, DataSourcesModel, ComponentTypesModel, ComponentHealthModel


__all__ = ['ComponentsModel']


class ComponentsModel(TableNameMixin, IdMixin, TimestampedMixin, ToDisplayMixin, BaseSiteModel):
    asset_id = Column(Integer, ForeignKey('assets.id', ondelete='CASCADE'), nullable=False)
    type_id = Column(Integer, ForeignKey('component_types.id', ondelete='CASCADE'), nullable=False)
    name = Column(String, nullable=False)
    install_date = Column(DateTime)

    asset = relationship('AssetsModel')
    data_sources = relationship('DataSourcesModel')
    type = relationship('ComponentTypesModel')
    health = relationship('ComponentHealthModel')
