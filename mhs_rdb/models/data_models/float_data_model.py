from sqlalchemy import Column, REAL

from ..base_model import BaseSiteModel
from .base_data_mixin import BaseDataMixin
from .data_statuses_mixin import DataStatusesMixin


__all__ = ['RawFloatDataModel', 'CalculatedFloatDataModel']


class BaseFloatData(BaseDataMixin):
    value = Column(REAL, nullable=False)


class RawFloatDataModel(BaseFloatData, DataStatusesMixin, BaseSiteModel):
    pass


class CalculatedFloatDataModel(BaseFloatData, BaseSiteModel):
    pass
