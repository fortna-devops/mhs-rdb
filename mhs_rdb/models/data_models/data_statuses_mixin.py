from typing import Sequence, ClassVar

from sqlalchemy import Column, SmallInteger, Index, func
from sqlalchemy.ext.declarative import declared_attr

from ..enums import DataStatusesEnum, get_statuses

__all__ = ['DataStatusesMixin']


class DataStatusesMixin:
    """
    `flags` field is a bit combination of possible statuses.
    Take a look to `DataStatusesEnum` for more info.
    """
    flags = Column(SmallInteger, nullable=False, server_default='0')

    __tablename__ = ClassVar[str]
    metric_id = ClassVar[Column]
    timestamp = ClassVar[Column]

    @property
    def statuses(self) -> Sequence[DataStatusesEnum]:
        return get_statuses(value=self.flags)

    @declared_attr
    def __table_args__(cls):  # pylint: disable=E0213
        return (
            Index(
                f'{cls.__tablename__}_metric_id_timestamp_minute_flags_index',
                cls.metric_id, func.date_trunc('minute', cls.timestamp), cls.flags
            ),
            Index(
                f'{cls.__tablename__}_metric_id_timestamp_hour_flags_index',
                cls.metric_id, func.date_trunc('hour', cls.timestamp), cls.flags
            )
        )
