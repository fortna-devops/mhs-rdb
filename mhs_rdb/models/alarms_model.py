from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, Float, ForeignKey, DateTime, String, ForeignKeyConstraint, Index, Enum
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship

from .mixins import TableNameMixin, BigIdMixin, TimestampedMixin
from .enums import ColorsEnum
from .base_model import BaseSiteModel
from ..utils.ranges import tsrange

if TYPE_CHECKING:
    from . import MetricsModel, AlarmConfigsModel, AlarmTypesModel


__all__ = ['AlarmsModel']


class AlarmsModel(TableNameMixin, BigIdMixin, TimestampedMixin, BaseSiteModel):

    @declared_attr
    def __table_args__(cls):  # pylint: disable=E0213
        return (
            ForeignKeyConstraint(
                ['type_name', 'color'],
                ['alarm_configs.type_name', 'alarm_configs.color'],
                ondelete='CASCADE'
            ),
            Index(
                f'{cls.__tablename__}_tsrange_metric_id_type_name_started_at_index',
                tsrange(cls.started_at, cls.ended_at, '[]'), cls.metric_id, cls.type_name, cls.started_at,
                postgresql_using='gist'
            ),
        )

    metric_id = Column(Integer, ForeignKey('metrics.id', ondelete='CASCADE'), nullable=False)
    started_at = Column(DateTime, nullable=False)
    ended_at = Column(DateTime, nullable=False)
    type_name = Column(String, nullable=False)
    color = Column(Enum(ColorsEnum), nullable=False)
    trigger_value = Column(Float, nullable=False)
    trigger_criteria = Column(Float, nullable=False)
    static_description = Column(String)

    metric = relationship('MetricsModel')
    alarm_config = relationship('AlarmConfigsModel')

    @property
    def description(self) -> str:

        if self.static_description:
            return self.static_description

        exceed = 'exceeded'
        exceed_value = self.trigger_value - self.trigger_criteria
        if self.trigger_value < self.trigger_criteria:
            exceed = 'was less than'
            exceed_value = self.trigger_criteria - self.trigger_value

        units = ''
        if self.metric.units is not None:
            units = f' {self.metric.units}'

        duration = (self.ended_at - self.started_at).total_seconds() / 60.0
        duration_units = 'minutes'
        if duration > 60:
            duration /= 60
            duration_units = 'hours'

        return f"{self.metric.display_name} {exceed} {self.trigger_criteria}{units} by an average of " \
               f"{exceed_value:.2f}{units} for {duration:.1f} consecutive {duration_units}"
