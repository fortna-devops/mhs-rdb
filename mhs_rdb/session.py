from sqlalchemy import orm


__all__ = ['Session']


class Session(orm.Session):

    def __enter__(self) -> 'Session':
        return self

    def __exit__(self, *_args, **_kwargs):
        self.close()

    def __del__(self):
        self.close()
