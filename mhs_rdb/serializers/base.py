from abc import ABC, abstractmethod
from typing import Any


__all__ = ['BaseSerializer']


class BaseSerializer(ABC):

    @abstractmethod
    def serialize(self, obj: Any) -> Any:
        return
