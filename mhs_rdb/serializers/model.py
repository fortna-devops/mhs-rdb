import logging
from typing import Any, Dict, Union, Callable, Iterable, List, ClassVar, Tuple

from sqlalchemy.orm import properties, relationships

from ..models.base_model import BaseModel
from .base import BaseSerializer


logger = logging.getLogger()


__all__ = ['ModelDictSerializer']


class ModelDictSerializer(BaseSerializer):
    _known_attr_types = (properties.ColumnProperty, relationships.RelationshipProperty)
    key_map: Dict[str, Union[Callable[[str, Any], str], str]] = {}

    included_attrs: ClassVar[Tuple[str, ...]] = ()
    excluded_attrs: ClassVar[Tuple[str, ...]] = ()

    def __init__(self):
        super(ModelDictSerializer, self).__init__()
        if self.included_attrs and self.excluded_attrs:
            raise AttributeError('Only "included_attrs" or "excluded_attrs" should be defined, not both.')

        if self.included_attrs:
            filter_function = lambda attr: attr.key in self.included_attrs
        elif self.excluded_attrs:
            filter_function = lambda attr: attr.key not in self.excluded_attrs
        else:
            filter_function = lambda attr: True

        self.__filter_function = filter_function

    def _map_key(self, key: str, obj: Any) -> str:
        mapped_key = self.key_map.get(key, key)
        if callable(mapped_key):
            key = mapped_key(key, obj)
        else:
            key = mapped_key

        return key

    def _serialize_attr(self, obj: Any,
                        attr: Union[properties.ColumnProperty, relationships.RelationshipProperty]) -> Any:
        key = attr.key
        value = getattr(obj, key)
        handler = getattr(self, f'handle_{key}', None)

        if handler is not None:
            value = handler(value)

        return value

    def _filter_attrs(self, attrs: Iterable[Any]) -> Iterable[Any]:  # pylint: disable=R0201
        return filter(self.__filter_function, attrs)

    def _serialize_model(self, obj: Any) -> Dict[str, Any]:
        res: Dict[str, Any] = {}
        for attr in self._filter_attrs(obj.__mapper__.attrs):
            if not isinstance(attr, self._known_attr_types):
                continue

            key = self._map_key(attr.key, obj)
            value = self._serialize_attr(obj, attr)
            res[key] = value

        return res

    def _serialize_list(self, objs: List[Any]) -> List[Dict[str, Any]]:
        return [self._serialize(obj) for obj in objs]

    def _serialize(self, obj: Any) -> Dict[str, Any]:
        if isinstance(obj, BaseModel):
            return self._serialize_model(obj)
        raise TypeError(f'Object of type {obj.__class__.__name__} is not serializable')

    def serialize(self, obj: Any) -> Union[Dict[str, Any], List[Dict[str, Any]]]:
        if isinstance(obj, list):
            return self._serialize_list(obj)
        return self._serialize(obj)
