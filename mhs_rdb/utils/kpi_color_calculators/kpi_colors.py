from typing import Dict

from ...models import ColorsEnum


__all__ = ['KpiColors']


class KpiColors:

    def __init__(self):
        self._colors: Dict[int, ColorsEnum] = {}

    def __repr__(self) -> str:
        return str(self._colors)

    def get_color(self, kpi_id: int) -> ColorsEnum:
        return self._colors.get(kpi_id, ColorsEnum.GREEN)

    def set_color(self, kpi_id: int, color: ColorsEnum):
        self._colors[kpi_id] = color
