from abc import ABC, abstractmethod
from typing import Sequence
from datetime import datetime

from sqlalchemy.orm import Session

from .kpi_colors import KpiColors


__all__ = ['BaseKpiColorCalculator']


class BaseKpiColorCalculator(ABC):
    """
    Base color calculator.
    """

    def __init__(self, session: Session):
        self._session = session

    @abstractmethod
    def calculate(self, alarm_start: datetime, alarm_end: datetime, asset_ids: Sequence[int] = ()) -> KpiColors:
        """
        Calculates colors for kpis.
        """
