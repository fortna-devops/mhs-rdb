from abc import ABC, abstractmethod
from typing import Dict, Optional, Sequence
from datetime import datetime

from sqlalchemy.orm import Session


__all__ = ['BaseHealthCalculator']


class BaseHealthCalculator(ABC):
    """
    Interface to calculate components health
    """

    def __init__(self, session: Session):
        self._session = session

    @abstractmethod
    def calculate(self, start_datetime: datetime, end_datetime: datetime,
                  component_ids: Optional[Sequence[int]] = None) -> Dict[int, float]:
        """
        Calculate health for components
        :param start_datetime: Lower limit of data timestamp
        :param end_datetime: Upper limit of data timestamp
        :param component_ids: Component ids to consider
        :return: Dict of component id as a key and health as a value
        """
