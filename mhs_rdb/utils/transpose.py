from typing import Dict, Sequence, Any

try:
    import pandas as pd
except ImportError:  # pragma: no cover
    raise ImportError('To transpose data you need to install `pandas`. '
                      'You can install it by adding mhs-rdb[pandas] to the requirements.')


__all__ = ['transpose']


def transpose(data: Sequence[Dict[str, Any]]) -> pd.DataFrame:
    """
    Convert data from row based metrics to column based

    :param data: Sequence of data points
    [
        {
            'asset_id': 1,
            'component_id': 1,
            'data_source_id': 1,
            'metric_id': 1,
            'timestamp': datetime.datetime(2020, 8, 28, 9, 36),
            'value': 219.997004191081
        },
        {
            'asset_id': 1,
            'component_id': 1,
            'data_source_id': 1,
            'metric_id': 1,
            'timestamp': datetime.datetime(2020, 8, 28, 9, 37),
            'value': 87.4480293818882
        },
        {
            'asset_id': 1,
            'component_id': 1,
            'data_source_id': 1,
            'metric_id': 1,
            'timestamp': datetime.datetime(2020, 8, 28, 9, 38),
            'value': 29.2964515686035
        },
        {
            'asset_id': 1,
            'component_id': 1,
            'data_source_id': 1,
            'metric_id': 2,
            'timestamp': datetime.datetime(2020, 8, 28, 9, 36),
            'value': 0.979949474334717
        },
        {
            'asset_id': 1,
            'component_id': 1,
            'data_source_id': 1,
            'metric_id': 2,
            'timestamp': datetime.datetime(2020, 8, 28, 9, 37),
            'value': 2.48629232247671
        },
        {
            'asset_id': 1,
            'component_id': 1,
            'data_source_id': 1,
            'metric_id': 3,
            'timestamp': datetime.datetime(2020, 8, 28, 9, 36),
            'value': 3.89001
        },
        {
            'asset_id': 1,
            'component_id': 1,
            'data_source_id': 1,
            'metric_id': 3,
            'timestamp': datetime.datetime(2020, 8, 28, 9, 37),
            'value': 2.25473
        },
        {
            'asset_id': 1,
            'component_id': 1,
            'data_source_id': 1,
            'metric_id': 3,
            'timestamp': datetime.datetime(2020, 8, 28, 9, 38),
            'value': 3.80423
        }
    ]
    :return: pandas DataFrame
    metric_id                     1         2        3
    timestamp
    2020-08-28 09:36:00  219.997004  0.979949  3.89001
    2020-08-28 09:37:00   87.448029  2.486292  2.25473
    2020-08-28 09:38:00   29.296452       NaN  3.80423
    """
    data_frame = pd.DataFrame(data)
    if data_frame.empty:
        return data_frame
    return data_frame.pivot(values='value', index='timestamp', columns='metric_id').infer_objects()  # type: ignore
