import logging
from collections import defaultdict
from typing import Dict, List, Tuple, Type, Iterator, Optional, Callable

from sqlalchemy.sql import elements

from ...models.data_models import DEFAULT_AGG_MAP
from ...models.data_models import TABLE_NAME_MAP, DataModelsType
from ...models import MetricsModel


__all__ = ['MetricQueryUnits']


logger = logging.getLogger()


QueryParameter = Optional[Callable[[elements.ColumnElement], elements.ColumnElement]]
QueryParameters = Tuple[Tuple[str, QueryParameter], ...]
GroupedMetrics = Dict[Tuple[Type[DataModelsType], QueryParameters, str], List[int]]
MetricsIterator = Iterator[Tuple[Type[DataModelsType], QueryParameters, str, List[int]]]


class MetricQueryUnits:
    """
    Container to store `MetricQueryUnit` objects
    """

    def __init__(self):
        self._grouped_metrics: GroupedMetrics = defaultdict(list)

    def add_metric(self, metric: MetricsModel, data_kinds: Tuple[str, ...],
                   query_parameters: Dict[str, QueryParameter] = None):
        for data_kind in data_kinds:
            table_name = getattr(metric, f'{data_kind}_data_table', None)
            if table_name is None:
                logger.warning(f'{data_kind}_data_table name is not set for metric with id={metric.id}. '
                               f'Will be skipped.')
                break

            data_model_cls = TABLE_NAME_MAP[table_name]

            if not query_parameters:
                query_parameters = {'value': DEFAULT_AGG_MAP[table_name]}

            key = (data_model_cls, tuple(sorted(query_parameters.items())), data_kind)
            self._grouped_metrics[key].append(metric.id)

    def __iter__(self) -> MetricsIterator:
        for (data_model_cls, query_parameters, data_kind), metric_ids in self._grouped_metrics.items():
            yield data_model_cls, query_parameters, data_kind, metric_ids
