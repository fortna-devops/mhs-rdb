from typing import Union, Type, Optional

from ...models import AssetsModel, ComponentsModel, DataSourcesModel, MetricsModel


__all__ = ['GroupLevel', 'GroupModels']


GroupModels = Type[Union[AssetsModel, ComponentsModel, DataSourcesModel, MetricsModel]]


class GroupLevel:
    """
    Group level wrapper for internal usage
    """

    def __init__(self, model: GroupModels, attr_name: Optional[str] = None):
        self.model = model
        self.attr_name = attr_name
