import logging
from collections import defaultdict
from typing import Union, Dict, List, Any, Iterator, Tuple, Optional, Type
from datetime import datetime

from ...models.data_models import TABLE_NAME_MAP, DataModelsType
from ...models import MetricsModel


__all__ = ['DataPoints']


logger = logging.getLogger()


class DataPoints:

    def __init__(self, data_kind: str):
        self._sorted_data: Dict[Type[DataModelsType], List[Dict[str, Any]]] = defaultdict(list)
        self._data_kind = data_kind

    @staticmethod
    def _get_row(metric_id: int, timestamp: datetime, value: Union[str, float, int],
                 flags: Optional[int]) -> Dict[str, Any]:
        row = {
            'metric_id': metric_id,
            'timestamp': timestamp,
            'value': value
        }
        if flags is not None:
            row['flags'] = flags

        return row

    def add_data(self, metric: MetricsModel, timestamp: datetime, value: Union[str, float, int],
                 flags: Optional[int] = None):

        table_name = getattr(metric, f'{self._data_kind}_data_table', None)
        if table_name is None:
            logger.warning(f'{self._data_kind}_data_table name is not set for metric with id={metric.id}. '
                           f'Will be skipped.')
            return

        data_model_cls = TABLE_NAME_MAP[table_name]
        row = self._get_row(metric_id=metric.id, timestamp=timestamp, value=value, flags=flags)
        self._sorted_data[data_model_cls].append(row)

    def __iter__(self) -> Iterator[Tuple[Type[DataModelsType], List[Dict[str, Any]]]]:
        for data_model_cls, rows in self._sorted_data.items():
            yield data_model_cls, rows

    def __len__(self) -> int:
        return sum(len(rows) for rows in self._sorted_data.values())
