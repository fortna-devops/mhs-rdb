from typing import List, Sequence, Dict, Any, Callable
from datetime import datetime

from sqlalchemy import Column
from sqlalchemy.orm import contains_eager

from .base import Base
from ..models import AlarmsModel, AlarmConfigsModel
from ..models import MetricsModel, KpisModel

from ..utils.ranges import tsrange


__all__ = ['Kpis']


class Kpis(Base):

    model = KpisModel

    def get_with_groups(self, asset_ids: Sequence[int] = ()) -> List[KpisModel]:

        query = self.query.options(
            contains_eager(self.model.kpi_group)
        )\
        .join(self.model.kpi_group)

        if asset_ids:
            query = query.filter(self.model.asset_id.in_(asset_ids))

        query = query.order_by(self.model.name)

        return query.all()

    def get_color_by_id(self,
                        agg_func_callback: Callable[[Column], Column],
                        alarm_start: datetime,
                        alarm_end: datetime,
                        asset_ids: Sequence[int] = ()) -> List[Dict[str, Any]]:

        column = KpisModel.id.label('id')
        query = self._session.query(
            agg_func_callback(AlarmsModel.color).label('color'),
            column
        )\
            .filter(AlarmConfigsModel.to_display)\
            .join(self.model.metrics)\
            .join(MetricsModel.alarms)\
            .join(AlarmsModel.alarm_config)\
            .group_by(column)

        query = query.filter(
            tsrange(
                AlarmsModel.started_at, AlarmsModel.ended_at, '[]'
            ).overlaps(
                tsrange(alarm_start, alarm_end, '[]')
            )
        )

        if asset_ids:
            query = query.filter(KpisModel.asset_id.in_(asset_ids))

        return self._query_rows_to_dicts(query)
