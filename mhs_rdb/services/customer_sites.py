from typing import List

from .base import Base
from .mixins import OrderByCreated
from ..models import CustomerSitesModel


__all__ = ['CustomerSites']


class CustomerSites(OrderByCreated, Base):
    model = CustomerSitesModel

    def get_schema_names(self) -> List[str]:
        return [site.schema_name for site in self.query.all()]
