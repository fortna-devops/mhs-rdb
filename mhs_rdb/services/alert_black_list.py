from datetime import datetime

from .base import Base
from .mixins import OrderByCreated
from ..models import AlertBlackListModel


__all__ = ['AlertBlackList']


class AlertBlackList(OrderByCreated, Base):

    model = AlertBlackListModel

    def delete_expired(self, expiration_datetime: datetime):
        self.delete(self.model.created_at < expiration_datetime)
