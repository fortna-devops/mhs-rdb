from typing import List, Sequence, Dict, Any, Optional, Union, Callable
from datetime import datetime

from sqlalchemy import Column, Date
from sqlalchemy.orm import contains_eager
from sqlalchemy.sql import func
from sqlalchemy.sql.expression import Label

from .base import Base
from ..models import AlarmsModel, ColorsEnum, AlarmConfigsModel
from ..models import MetricsModel, DataSourcesModel, ComponentsModel

from ..utils.ranges import tsrange, daterange


__all__ = ['Alarms']


class Alarms(Base):

    model = AlarmsModel

    @staticmethod
    def _trim_dates(alarms: List[AlarmsModel], start: datetime, end: datetime) -> List[AlarmsModel]:

        for alarm in alarms:

            if alarm.started_at < start:
                alarm.started_at = start

            if alarm.ended_at > end:
                alarm.ended_at = end

        return alarms

    def _insert(self, alarm: AlarmsModel):
        """
        Inserts new alarm in case if there is no time frame intersection
        for the specific metric_id and type_name. Otherwise deletes intersected alarms.
        """

        existing_alarms = self.query.filter(
            alarm.metric_id == self.model.metric_id,
            tsrange(
                alarm.started_at, alarm.ended_at, '[]'
            ).overlaps(
                tsrange(self.model.started_at, self.model.ended_at, '[]')
            ),
            alarm.type_name == self.model.type_name
        ).all()

        if not existing_alarms:
            self._session.add(alarm)
            return

        first_alarm, last_alarm = existing_alarms[0], existing_alarms[0]
        for existing_alarm in existing_alarms[1:]:
            if first_alarm.started_at > existing_alarm.started_at:
                first_alarm = existing_alarm
            if last_alarm.ended_at < existing_alarm.ended_at:
                last_alarm = existing_alarm

        for existing_alarm in existing_alarms:
            self._session.delete(existing_alarm)

        first_alarm, last_alarm = first_alarm.clone(), last_alarm.clone()
        first_alarm.id, last_alarm.id = None, None

        if alarm.color == first_alarm.color:
            alarm.started_at = min(alarm.started_at, first_alarm.started_at)
        elif alarm.started_at > first_alarm.started_at:
            first_alarm.ended_at = alarm.started_at
            self._session.add(first_alarm)

        if alarm.color == last_alarm.color:
            alarm.ended_at = max(alarm.ended_at, last_alarm.ended_at)
        elif alarm.ended_at < last_alarm.ended_at:
            last_alarm.started_at = alarm.ended_at
            self._session.add(last_alarm)

        self._session.add(alarm)

    def get_color_by_component_id(self,
                                  start: datetime,
                                  end: datetime,
                                  agg_func_callback: Callable[[Column], Column],
                                  component_ids: Optional[Sequence[int]] = None) -> List[Dict[str, Any]]:

        component_id_column = DataSourcesModel.component_id.label('component_id')
        query = self._session.query(
            agg_func_callback(self.model.color).label('color'),
            component_id_column
        )\
            .filter(AlarmConfigsModel.to_display)\
            .join(self.model.alarm_config)\
            .join(self.model.metric)\
            .join(MetricsModel.data_source)\
            .group_by(component_id_column)

        query = query.filter(
            tsrange(
                self.model.started_at, self.model.ended_at, '[]'
            ).overlaps(
                tsrange(start, end, '[]')
            )
        )

        if component_ids:
            query = query.filter(component_id_column.in_(component_ids))

        return self._query_rows_to_dicts(query)

    def filter(self,
               start: datetime,
               end: datetime,
               asset_ids: Optional[Sequence[int]] = None,
               component_ids: Optional[Sequence[int]] = None,
               data_source_ids: Optional[Sequence[int]] = None,
               metric_ids: Optional[Sequence[int]] = None,
               to_display: Optional[bool] = True,
               colors: Optional[Sequence[ColorsEnum]] = None,
               type_names: Optional[Sequence[str]] = None,
               offset: Optional[int] = None,
               limit: Optional[int] = None,
               count: bool = False) -> Union[int, List[AlarmsModel]]:

        query = self.query\
            .options(
                contains_eager(self.model.alarm_config),
                contains_eager(
                    self.model.metric,
                    MetricsModel.data_source,
                    DataSourcesModel.component,
                    ComponentsModel.asset
                ),
                contains_eager(
                    self.model.metric,
                    MetricsModel.data_source,
                    DataSourcesModel.type
                ),
                contains_eager(
                    self.model.metric,
                    MetricsModel.data_source,
                    DataSourcesModel.component,
                    ComponentsModel.type
                )
            )\
            .join(self.model.alarm_config)\
            .join(self.model.metric)\
            .join(MetricsModel.data_source)\
            .join(DataSourcesModel.component)\
            .join(ComponentsModel.asset)\
            .join(DataSourcesModel.type)\
            .join(ComponentsModel.type)

        query = query.filter(
            tsrange(
                self.model.started_at, self.model.ended_at, '[]'
            ).overlaps(
                tsrange(start, end, '[]')
            )
        )

        if asset_ids:
            query = query.filter(ComponentsModel.asset_id.in_(asset_ids))

        if component_ids:
            query = query.filter(DataSourcesModel.component_id.in_(component_ids))

        if data_source_ids:
            query = query.filter(MetricsModel.data_source_id.in_(data_source_ids))

        if metric_ids:
            query = query.filter(self.model.metric_id.in_(metric_ids))

        if to_display is not None:
            query = query.filter(AlarmConfigsModel.to_display.is_(to_display))

        if colors:
            query = query.filter(self.model.color.in_(colors))

        if type_names:
            query = query.filter(self.model.type_name.in_(type_names))

        query = query.order_by(self.model.metric_id, self.model.type_name, self.model.started_at)

        if offset:
            query = query.offset(offset)

        if limit:
            query = query.limit(limit)

        if count:
            return query.count()

        return self._trim_dates(query.all(), start, end)

    def count(self,
              group_by: Sequence[Label],
              start: datetime,
              end: datetime,
              asset_ids: Optional[Sequence[int]] = None,
              component_ids: Optional[Sequence[int]] = None,
              data_source_ids: Optional[Sequence[int]] = None,
              metric_ids: Optional[Sequence[int]] = None,
              to_display: Optional[bool] = True) -> List[Dict[str, Any]]:

        query = self._session.query(
            *group_by,
            func.count(self.model.id).label('num_alarms')
        )\
            .join(self.model.alarm_config)\
            .join(self.model.metric)\
            .join(MetricsModel.data_source)\
            .join(DataSourcesModel.component)\
            .join(ComponentsModel.asset)\
            .group_by(*group_by)

        query = query.filter(
            tsrange(
                self.model.started_at, self.model.ended_at, '[]'
            ).overlaps(
                tsrange(start, end, '[]')
            )
        )

        if asset_ids:
            query = query.filter(ComponentsModel.asset_id.in_(asset_ids))

        if component_ids:
            query = query.filter(DataSourcesModel.component_id.in_(component_ids))

        if data_source_ids:
            query = query.filter(MetricsModel.data_source_id.in_(data_source_ids))

        if metric_ids:
            query = query.filter(self.model.metric_id.in_(metric_ids))

        if to_display is not None:
            query = query.filter(AlarmConfigsModel.to_display.is_(to_display))

        return self._query_rows_to_dicts(query)

    def count_by_date(self,
                      group_by: Sequence[Label],
                      start: datetime,
                      end: datetime,
                      timezone: str = '0',
                      asset_ids: Optional[Sequence[int]] = None,
                      component_ids: Optional[Sequence[int]] = None,
                      data_source_ids: Optional[Sequence[int]] = None,
                      metric_ids: Optional[Sequence[int]] = None,
                      to_display: Optional[bool] = True) -> List[Dict[str, Any]]:

        series = self._session.query(
            func.generate_series(start, end, '1 day').label('timestamp')
        ).subquery('time_series')

        timezone_op = 'at time zone'

        on_clause = daterange(
            AlarmsModel.started_at.op(timezone_op)(timezone).cast(Date),
            AlarmsModel.ended_at.op(timezone_op)(timezone).cast(Date),
            '[]'
        ).contains(series.c.timestamp.op(timezone_op)(timezone).cast(Date))

        query = self._session.query(
            series.c.timestamp.op(timezone_op)(timezone).cast(Date).label('date'),
            *group_by,
            func.count(self.model.id).label('num_alarms')
        )\
            .outerjoin(self.model, on_clause)\
            .join(self.model.alarm_config)\
            .join(self.model.metric)\
            .join(MetricsModel.data_source)\
            .join(DataSourcesModel.component)\
            .join(ComponentsModel.asset)\
            .group_by(series.c.timestamp, *group_by)\
            .order_by(series.c.timestamp)

        query = query.filter(
            tsrange(
                self.model.started_at, self.model.ended_at, '[]'
            ).overlaps(
                tsrange(start, end, '[]')
            )
        )

        if asset_ids:
            query = query.filter(ComponentsModel.asset_id.in_(asset_ids))

        if component_ids:
            query = query.filter(DataSourcesModel.component_id.in_(component_ids))

        if data_source_ids:
            query = query.filter(MetricsModel.data_source_id.in_(data_source_ids))

        if metric_ids:
            query = query.filter(self.model.metric_id.in_(metric_ids))

        if to_display is not None:
            query = query.filter(AlarmConfigsModel.to_display.is_(to_display))

        return self._query_rows_to_dicts(query)

    def insert(self, instance: AlarmsModel):
        self._insert(instance)
        self._session.commit()

    def insert_all(self, instances: Sequence[AlarmsModel], batch_size: int = 0):
        for alarm in instances:
            self.insert(alarm)
