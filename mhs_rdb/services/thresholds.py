from .base import Base
from .mixins import OrderByCreated
from ..models import ThresholdsModel


__all__ = ['Thresholds']


class Thresholds(OrderByCreated, Base):
    model = ThresholdsModel
