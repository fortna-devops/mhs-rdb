from .base import Base
from .mixins import OrderByCreated

from ..models import ComponentTypesModel


__all__ = ['ComponentTypes']


class ComponentTypes(OrderByCreated, Base):
    model = ComponentTypesModel
