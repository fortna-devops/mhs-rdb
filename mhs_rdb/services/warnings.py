from .base import Base
from ..models import WarningsModel


__all__ = ['Warnings']


class Warnings(Base):

    model = WarningsModel
