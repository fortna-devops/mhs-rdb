from typing import Tuple, List, Sequence, Optional

from sqlalchemy.orm import contains_eager

from .base import Base
from .mixins import OrderByCreated

from ..models import DataSourcesModel, MetricsModel, ThresholdsModel


__all__ = ['DataSources']


class DataSources(OrderByCreated, Base):

    model = DataSourcesModel

    def get_ids(self, type_names: Tuple[str, ...] = ()) -> List[int]:

        query = self._session.query(self.model.id)\
            .join(self.model.type)

        if type_names:
            query = query.filter(self.model.type.name.in_(type_names))

        query = query.order_by(self.model.id)

        return [data_source['id'] for data_source in self._query_rows_to_dicts(query)]

    def get_with_metrics_and_thresholds(self,
                                        ids: Optional[Sequence[int]] = None,
                                        metric_to_analyze: Optional[bool] = None,
                                        threshold_type_names: Optional[Sequence[str]] = None) -> List[DataSourcesModel]:

        query = self.query\
            .options(
                contains_eager(self.model.metrics, MetricsModel.thresholds, ThresholdsModel.level),
                contains_eager(self.model.type)
            )\
            .join(self.model.metrics)\
            .join(MetricsModel.thresholds)\
            .join(ThresholdsModel.level)\
            .join(self.model.type)

        if ids:
            query = query.filter(self.model.id.in_(ids))

        if metric_to_analyze is not None:
            query = query.filter(MetricsModel.to_analyze == metric_to_analyze)

        if threshold_type_names:
            query = query.filter(ThresholdsModel.type_name.in_(threshold_type_names))

        return query.all()
