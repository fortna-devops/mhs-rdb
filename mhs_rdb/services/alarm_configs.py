from .base import Base
from ..models import AlarmConfigsModel


__all__ = ['AlarmConfigsModel']


class AlarmConfigs(Base):
    model = AlarmConfigsModel
