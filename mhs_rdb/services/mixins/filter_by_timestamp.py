from datetime import datetime
from typing import List, Any, Optional

from ..base import Base

__all__ = ['FilterByTimestamp']


class FilterByTimestamp(Base):  # pylint: disable=W0223

    def filter_by_timestamp(self, start: datetime, end: datetime,
                            sensor_ids: Optional[List[int]] = None, limit: Optional[int] = None) -> List[Any]:
        assert start <= end, 'start timestamp should be less or equal than end timestamp'

        query = self.query.filter(self.model.timestamp.between(start, end))\
            .order_by(self.model.timestamp.desc())

        if sensor_ids is not None:
            query = query.filter(
                self.model.sensor_id.in_(sensor_ids)
            )

        if limit is not None:
            query = query.limit(limit)

        return query.all()
