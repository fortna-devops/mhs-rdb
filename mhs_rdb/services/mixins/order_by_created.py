from typing import List, Any

from ..base import Base

__all__ = ['OrderByCreated']


class OrderByCreated(Base):  # pylint: disable=W0223

    def get_all(self) -> List[Any]:
        return self.query.order_by(self.model.created_at).all()
