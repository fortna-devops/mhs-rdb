import sys


VERSION = (2, 2, 4)
__version__ = '.'.join(str(v) for v in VERSION)


# It will be better just to remove those imports.
# But it will require to update every place in project that use this lib.
if 'setup.py' not in sys.argv[0]:
    from mhs_rdb.config_manager import ConfigManager
    from mhs_rdb.connector import Connector
    from mhs_rdb.services import *
