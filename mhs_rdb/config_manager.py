from typing import Optional, Dict, List

from boto3.session import Session as BotoSession

from .connector import Connector
from .services import CustomerSites


HOSTS: Dict[str, str] = {
    'develop_new': 'mhspredict-dev-new.c2z9g3wxqyud.us-east-1.rds.amazonaws.com',
    'preprod': 'mhspredict-prod.cluster-c2z9g3wxqyud.us-east-1.rds.amazonaws.com',
    'master': 'mhspredict-prod.cluster-c2z9g3wxqyud.us-east-1.rds.amazonaws.com'
}

DATABASES: Dict[str, List[str]] = {
    'develop_new': [
        'mhs',
        'dhl'
    ],
    'preprod': [
        'fedex',
        'dhl',
        'amazon',
        'ups'
    ],
    'master': [
        'fedex',
        'dhl',
        'amazon',
        'ups'
    ]
}

CUSTOMER_SCHEMA_NAME = 'customer'


class ConfigManager:

    def __init__(self, env: str, boto_session: Optional[BotoSession] = None, debug: bool = False):
        self._env = env
        if not boto_session:
            boto_session = BotoSession()
        self._connector = Connector(boto_session=boto_session, host=HOSTS[self._env], debug=debug)

    @property
    def connector(self) -> Connector:
        return self._connector

    @property
    def customers(self) -> List[str]:
        return DATABASES[self._env]

    @property
    def customer_sites(self) -> Dict[str, List[str]]:
        customer_sites = {}
        for customer in DATABASES[self._env]:
            customer_sites[customer] = self.sites(customer)
        return customer_sites

    def sites(self, customer: str) -> List[str]:
        with self._connector.get_session(customer, CUSTOMER_SCHEMA_NAME) as session:
            return CustomerSites(session).get_schema_names()
