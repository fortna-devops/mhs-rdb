# Connector

Instance of the `Connector` must be assigned to the global variable 
and then used inside the lambda. 

```python
BOTO_SESSION = boto3.Session(profile_name=os.getenv('profile'))
CONNECTOR = Connector(boto_session=BOTO_SESSION, host=RDB_HOST, username=RDB_USERNAME)
```

The next params for the `Connector` are optional and could be set 
via the environment with the next prefix `AWS_RDB_`:

* `host` - `AWS_RDB_HOST` 
* `port` - `AWS_RDB_PORT`
* `username` - `AWS_RDB_USERNAME`

# Session

Once the global `Connector` is initialized we can get `session` for 
further usage with services.

```python
import boto3

from mhs_rdb import Connector
from mhs_rdb import Sensors

BOTO_SESSION = boto3.Session(profile_name=os.getenv('profile'))
CONNECTOR = Connector(boto_session=BOTO_SESSION)

DATABASE = 'mhspredict_dev_demo'
SITE = 'mhspredict_site_mhs_dataart'

def main():
    db_session = CONNECTOR.get_session(DATABASE, SITE)
    sensors_service = Sensors(db_session)
    for sensor in sensors_service.get_all():
        print(sensor.name)
    
``` 
