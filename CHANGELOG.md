# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.5] - 2020-02-27

### Changed
- `JobStateEnum` and `ActionTypeEnum` attribute names were changed to match what was in the schema migration file

## [1.1.4] - 2020-02-24

### Fixed
- Filter by `alarms_history`.`data_timestamp` instead of by `alarms`.`data_timestamp` in `Alarms`.`count_by_date()`.

## [1.1.2] - 2020-02-21

### Add
- Add `rnd` environment support.

## [1.1.1] - 2020-02-19

### Add
- Create `AlarmsHistory` service.

## [1.1.0] - 2020-02-19

### Add
- Set up mhs-rdb CI process.

### Fixed
- Correct all type annotations.
- Improve code quality adhering to coding standards.

## [1.0.42] - 2020-02-13

### Add
- Create `PLCDataDynamicModel` and `PLCDataDynamic` service.

## [1.0.41] - 2020-02-11

### Add
- Add Frame and Pin_Divert to `EquipmentEnum`

## [1.0.40] - 2020-02-06

### Add
- Create `RobotActionsModel`, `RobotJobsModel`, `RobotMissionsModel` models.
- Create `RobotActions`, `RobotJobs`, and `RobotMissions` services.
- Create `JobStateEnum` and `ActionTypeEnum` enums.
- `MiR500` to `EquipmentEnum` type.

## [1.0.39] - 2020-01-31

### Add
- `hf_rms_acceleration_x` and `hf_rms_acceleration_z` to `AlarmsMetricEnum`

## [1.0.21] - 2019-11-26

### Add
- `ISO_SPIKE` & `ISO_PERSISTENT` to `AlarmsTypeEnum`
