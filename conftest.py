import boto3
import pytest
from moto import mock_rds
from alchemy_mock.mocking import UnifiedAlchemyMagicMock


from mhs_rdb.config_manager import ConfigManager


@pytest.fixture(scope='function')
def aws_credentials(monkeypatch):
    monkeypatch.setenv('AWS_ACCESS_KEY_ID', 'testing')
    monkeypatch.setenv('AWS_SECRET_ACCESS_KEY', 'testing')
    monkeypatch.setenv('AWS_SECURITY_TOKEN', 'testing')
    monkeypatch.setenv('AWS_SESSION_TOKEN', 'testing')
    monkeypatch.setenv('AWS_DEFAULT_REGION', 'us-east-1')


@pytest.fixture(scope='function')
def rds(aws_credentials):
    with mock_rds():
        yield boto3.client('rds')


@pytest.fixture(scope='function')
def session_execute(mocker):
    return mocker.patch('mhs_rdb.session.Session.execute')


@pytest.fixture(scope='function')
def db_session():
    return UnifiedAlchemyMagicMock()


@pytest.fixture(scope='function')
def cm_config(rds, mocker):
    class CMConfig:
        env = 'test_env'
        host = 'test_host'
        customer = 'test_customer'
        customers = [customer]
        sites = ['test_site']
    mocker.patch.dict('mhs_rdb.config_manager.HOSTS', {CMConfig.env: CMConfig.host})
    mocker.patch.dict('mhs_rdb.config_manager.DATABASES', {CMConfig.env: CMConfig.customers})

    schema_names_mock = mocker.patch('mhs_rdb.services.CustomerSites.get_schema_names')
    schema_names_mock.return_value = CMConfig.sites
    CMConfig.schema_names_mock = schema_names_mock

    return CMConfig


@pytest.fixture(scope='function')
def config_manager(cm_config):
    return ConfigManager(cm_config.env)
