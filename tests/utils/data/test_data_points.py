from datetime import datetime

import pytest

from mhs_rdb.models import MetricsModel
from mhs_rdb.models.data_models import RAW_FLOAT_DATA_TABLE, RAW_INT_DATA_TABLE, RAW_STRING_DATA_TABLE,\
    CALCULATED_FLOAT_DATA_TABLE, CALCULATED_INT_DATA_TABLE, CALCULATED_STRING_DATA_TABLE,\
    RawFloatDataModel, RawIntDataModel, RawStringDataModel,\
    CalculatedFloatDataModel, CalculatedIntDataModel, CalculatedStringDataModel
from mhs_rdb.utils.data import DataPoints, DataProxy

from tests.utils.data import DATA_TIMESTAMP, get_insert_data


@pytest.mark.parametrize('count', (0, 1, 5))
def test_data_points_len(count):
    data_points = DataPoints(DataProxy.raw_data_kind)

    for i in range(1, count+1):
        metric = MetricsModel(id=i)
        setattr(metric, f'{DataProxy.raw_data_kind}_data_table', RAW_FLOAT_DATA_TABLE)
        data_points.add_data(metric=metric, timestamp=datetime.utcnow(), value=i*0.1)

    assert len(data_points) == count


iter_data_cases = (
    ([], [], [], [], [], DataProxy.raw_data_kind, {}),

    ([1], [DATA_TIMESTAMP], [0.2], [0], [RAW_FLOAT_DATA_TABLE], DataProxy.raw_data_kind, {RawFloatDataModel: [{'metric_id': 1, 'value': 0.2, 'flags': 0, 'timestamp': DATA_TIMESTAMP}]}),
    ([2], [DATA_TIMESTAMP], [5], [0], [RAW_INT_DATA_TABLE], DataProxy.raw_data_kind, {RawIntDataModel: [{'metric_id': 2, 'value': 5, 'flags': 0, 'timestamp': DATA_TIMESTAMP}]}),
    ([3], [DATA_TIMESTAMP], ['test'], [0], [RAW_STRING_DATA_TABLE], DataProxy.raw_data_kind, {RawStringDataModel: [{'metric_id': 3, 'value': 'test', 'flags': 0, 'timestamp': DATA_TIMESTAMP}]}),
    ([1, 2, 3], [DATA_TIMESTAMP, DATA_TIMESTAMP, DATA_TIMESTAMP], [0.2, 5, 'test'], [0, 0, 0], [RAW_FLOAT_DATA_TABLE, RAW_INT_DATA_TABLE, RAW_STRING_DATA_TABLE], DataProxy.raw_data_kind, {RawFloatDataModel: [{'metric_id': 1, 'value': 0.2, 'flags': 0, 'timestamp': DATA_TIMESTAMP}], RawIntDataModel: [{'metric_id': 2, 'value': 5, 'flags': 0, 'timestamp': DATA_TIMESTAMP}], RawStringDataModel: [{'metric_id': 3, 'value': 'test', 'flags': 0, 'timestamp': DATA_TIMESTAMP}]}),
    ([1, 2], [DATA_TIMESTAMP, DATA_TIMESTAMP], [0.2, 0.3], [0, 0], [RAW_FLOAT_DATA_TABLE, RAW_FLOAT_DATA_TABLE], DataProxy.raw_data_kind, {RawFloatDataModel: [{'metric_id': 1, 'value': 0.2, 'flags': 0, 'timestamp': DATA_TIMESTAMP}, {'metric_id': 2, 'value': 0.3, 'flags': 0, 'timestamp': DATA_TIMESTAMP}]}),

    ([1], [DATA_TIMESTAMP], [0.2], [None], [CALCULATED_FLOAT_DATA_TABLE], DataProxy.calculated_data_kind, {CalculatedFloatDataModel: [{'metric_id': 1, 'value': 0.2, 'timestamp': DATA_TIMESTAMP}]}),
    ([2], [DATA_TIMESTAMP], [5], [None], [CALCULATED_INT_DATA_TABLE], DataProxy.calculated_data_kind, {CalculatedIntDataModel: [{'metric_id': 2, 'value': 5, 'timestamp': DATA_TIMESTAMP}]}),
    ([3], [DATA_TIMESTAMP], ['test'], [None], [CALCULATED_STRING_DATA_TABLE], DataProxy.calculated_data_kind, {CalculatedStringDataModel: [{'metric_id': 3, 'value': 'test', 'timestamp': DATA_TIMESTAMP}]}),
    ([1, 2, 3], [DATA_TIMESTAMP, DATA_TIMESTAMP, DATA_TIMESTAMP], [0.2, 5, 'test'], [None, None, None], [CALCULATED_FLOAT_DATA_TABLE, CALCULATED_INT_DATA_TABLE, CALCULATED_STRING_DATA_TABLE], DataProxy.calculated_data_kind, {CalculatedFloatDataModel: [{'metric_id': 1, 'value': 0.2, 'timestamp': DATA_TIMESTAMP}], CalculatedIntDataModel: [{'metric_id': 2, 'value': 5, 'timestamp': DATA_TIMESTAMP}], CalculatedStringDataModel: [{'metric_id': 3, 'value': 'test', 'timestamp': DATA_TIMESTAMP}]}),
    ([1, 2], [DATA_TIMESTAMP, DATA_TIMESTAMP], [0.2, 0.3], [None, None], [CALCULATED_FLOAT_DATA_TABLE, CALCULATED_FLOAT_DATA_TABLE], DataProxy.calculated_data_kind, {CalculatedFloatDataModel: [{'metric_id': 1, 'value': 0.2, 'timestamp': DATA_TIMESTAMP}, {'metric_id': 2, 'value': 0.3, 'timestamp': DATA_TIMESTAMP}]}),
)


@pytest.mark.parametrize('metric_ids, timestamps, values, flags, data_tables, data_kind, expected_result', iter_data_cases)
def test_data_points_iter_data(metric_ids, timestamps, values, flags, data_tables, data_kind, expected_result):
    data_points = DataPoints(data_kind)

    for data_table, row in get_insert_data(metric_ids, timestamps, values, flags, data_tables):
        metric = MetricsModel(id=row.pop('metric_id'))
        setattr(metric, f'{data_kind}_data_table', data_table)
        data_points.add_data(metric=metric, **row)

    assert dict(data_points) == expected_result
