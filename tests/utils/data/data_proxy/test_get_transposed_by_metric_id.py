from datetime import datetime, timedelta

import pytest
import pandas as pd

from mhs_rdb.utils.data import DataProxy, MetricQueryUnits

from tests.utils.data import QUERY_TEST_DATA, get_data_dicts, get_metrics


@pytest.mark.parametrize('metric_ids, timestamps, values, data_tables, data_kind', QUERY_TEST_DATA)
def test_get_transposed_by_metric_id(mocker, db_session, metric_ids, timestamps, values, data_tables, data_kind):
    end_datetime = datetime.utcnow()
    start_datetime = end_datetime - timedelta(minutes=1)
    sample_rate = DataProxy.hour_sample_rate

    expected_df = pd.DataFrame([values], columns=metric_ids, index=list(set(timestamps)))

    get_data_mock = mocker.patch('mhs_rdb.utils.data.DataProxy.get_data')
    get_data_mock.return_value = get_data_dicts(metric_ids, timestamps, values)

    get_metrics_mock = mocker.patch('mhs_rdb.utils.data.DataProxy._get_metric_query_units')
    query_units = MetricQueryUnits()
    for metric in get_metrics(metric_ids, data_tables, data_kind):
        query_units.add_metric(metric, (data_kind,))
    get_metrics_mock.return_value = query_units

    data = DataProxy(db_session).get_transposed_by_metric_id(
        start_datetime=start_datetime, end_datetime=end_datetime, metric_ids=metric_ids, sample_rate=sample_rate
    )

    assert data.equals(expected_df)
